package com.example.temperatureconverter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TemperatureHistory";
    private int mySelectedConversionScaleId;
    private TextView myHistoryTextView;
    private EditText myInputTemperatureTextView;
    private EditText myResultTemperatureTextView;
    private Toast myToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing class members
        mySelectedConversionScaleId = ((RadioGroup) findViewById(R.id.TemperatureOptionRadioGroup)).getCheckedRadioButtonId();
        myResultTemperatureTextView = (EditText) findViewById(R.id.ResultTemperatureEditText);
        myToast = null;

        //Setting scrolling behavior
        myHistoryTextView = (TextView) findViewById(R.id.ConversionHistory);
        myHistoryTextView.setMovementMethod(new ScrollingMovementMethod());

        //setting filter as decimal places of the input must be restricted
        myInputTemperatureTextView = (EditText) findViewById(R.id.InputTemperatureEditText);
        myInputTemperatureTextView.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(1)});

        //initialize conversionbutton click behavior
        Button conversionButton = (Button) findViewById(R.id.ConvertButton);
        conversionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myResultTemperatureTextView.getText().clear();
                String inputTemperatureText = myInputTemperatureTextView.getText().toString();
                calculateTemperature(inputTemperatureText);
            }

        });


    }

    //User input temperature scale is set from selected radio button id
    public void setTemperatureScale(View view) {
        RadioButton selectedRadioButton = (RadioButton) view;
        mySelectedConversionScaleId = selectedRadioButton.getId();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(TAG, myHistoryTextView.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        myHistoryTextView.setText(savedInstanceState.getString(TAG));
    }

    //Calculates the Temperature based on the temperature scale selected
    private void calculateTemperature(String inputTemperatureText) {

        if (inputTemperatureText.isEmpty()) {
            showAToast(R.string.UserInputRequired);
            return;
        } else if (inputTemperatureText.length() == 1) {
            boolean invalidInput = inputTemperatureText.contentEquals(".") || inputTemperatureText.contentEquals("-");
            if (invalidInput) {
                showAToast(R.string.InvalidInput);
                return;
            }
        } else if (inputTemperatureText.length() == 2) {
            if (inputTemperatureText.contentEquals("-.")) {
                showAToast(R.string.InvalidInput);
                return;
            }
        }

        double inputTemperatureValue = Double.parseDouble(inputTemperatureText);
        double resultTemperatureValue = 0.0;
        String resultString;

        if (mySelectedConversionScaleId == R.id.FToCRadioButton) {
            resultTemperatureValue = (inputTemperatureValue - 32.0) * (5.0 / 9.0);
            resultString = getString(R.string.TemperatureScaleTagFtoC);
        } else {
            resultTemperatureValue = (inputTemperatureValue * 9.0 / 5.0) + 32.0;
            resultString = getString(R.string.TemperatureScaleTagCtoF);
            ;
        }
        String outputString = String.format(" %.1f", resultTemperatureValue);
        resultString += outputString;

        //Updates the current conversion to the conversion history list
        myResultTemperatureTextView.setText(outputString);
        String currentHistoryText = myHistoryTextView.getText().toString();
        myHistoryTextView.setText("\n" + resultString + "\n" + currentHistoryText);
    }

    private void showAToast(int toastMessage) {
        //checking if already toast is displayed and is of the same type.
        //Avoiding continues Toast message que
        if (myToast != null && myToast.getView().getWindowVisibility() == View.VISIBLE) {
            String displayedText = ((TextView) ((LinearLayout) myToast.getView()).getChildAt(0)).getText().toString();
            if (displayedText.equals(getString(toastMessage))) {
                return;
            }
        }
        myToast = Toast.makeText(this, getString(toastMessage), Toast.LENGTH_SHORT);

        myToast.show();
    }
}

class DecimalDigitsInputFilter implements InputFilter {

    private int myNumberOfDecimalDigits;

    public DecimalDigitsInputFilter(int numberOfDecimalDigits) {
        myNumberOfDecimalDigits = numberOfDecimalDigits;
    }

    @Override
    public CharSequence filter(CharSequence source,
                               int start,
                               int end,
                               Spanned dest,
                               int dstart,
                               int dend) {


        int dotPosition = -1;
        int length = dest.length();
        for (int i = 0; i < length; i++) {
            char c = dest.charAt(i);
            if (c == '.') {
                dotPosition = i;
                break;
            }
        }
        if (dotPosition >= 0) {
            // if the text is entered before the dot
            if (dend <= dotPosition) {
                return null;
            }
            // limiting the number of digits after decimalpoint
            if (length - dotPosition > myNumberOfDecimalDigits) {
                return "";
            }
        }

        return null;
    }

}

package com.example.notepad;

import java.io.Serializable;

public class Note implements Serializable{
    private String updateTime;
    private String NoteText;

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getNoteText() {
        return NoteText;
    }

    public void setNoteText(String noteText) {
        NoteText = noteText;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

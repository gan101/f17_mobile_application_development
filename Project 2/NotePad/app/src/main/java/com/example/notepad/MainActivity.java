package com.example.notepad;

import android.annotation.TargetApi;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.ULocale;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@TargetApi(24)
public class MainActivity extends AppCompatActivity {
    private Note myNote;
    private EditText myNoteEditTextView;
    private TextView myNoteUpdateTimeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        myNoteUpdateTimeTextView = (TextView) findViewById(R.id.UpdateTimeTextView);
        myNoteEditTextView = (EditText) findViewById(R.id.NoteEditTextView);
        myNoteEditTextView.setMovementMethod(new ScrollingMovementMethod());


    }

    @Override
    protected void onResume() {
        super.onResume();
        // load saved data
        boolean success = LoadNote();
        if (success) {
            myNoteUpdateTimeTextView.setText(myNote.getUpdateTime());
            myNoteEditTextView.setText(myNote.getNoteText());
        } else {
            myNoteUpdateTimeTextView.setText(getCurrentTime());
        }

    }

    private boolean LoadNote() {
        File file = getApplicationContext().getFileStreamPath(getString(R.string.FileName));
        if (!file.exists())
            return false;
        else {
            try {
                FileInputStream fileInputStream = openFileInput(getString(R.string.FileName));
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                myNote = (Note) objectInputStream.readObject();
                return true;

            } catch (FileNotFoundException e) {
                Toast.makeText(this, getString(R.string.FileNotFound), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } catch (IOException e) {
                Log.d(getString(R.string.ErrorMessage), e.getMessage());
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                Log.d(getString(R.string.ErrorMessage), e.getMessage());
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        String noteText = myNoteEditTextView.getText().toString();

        myNote = new Note();
        myNote.setNoteText(noteText);
        myNote.setUpdateTime(getCurrentTime());


        //save data
        saveNote();

    }

    private void saveNote() {
        try {
            FileOutputStream fileOutputStream = openFileOutput(getString(R.string.FileName), Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(myNote);

            objectOutputStream.flush();
            fileOutputStream.close();


        } catch (FileNotFoundException e) {
            Toast.makeText(this, getString(R.string.FileNotFound), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(getString(R.string.ErrorMessage), e.getMessage());
            e.printStackTrace();
        }
    }

    @NonNull
    private String getCurrentTime() {
        //Calender is required to get the device time
        Calendar calender = Calendar.getInstance();

        //SimpleDateFormat formats the time obtained from calender to our specific format
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d yyyy hh:mm:ss aaa", ULocale.getDefault());
        String updateTime = simpleDateFormat.format(calender.getTime());


        updateTime = getString(R.string.UpdateTimeTextView) + " " + updateTime;
        return updateTime;
    }
}

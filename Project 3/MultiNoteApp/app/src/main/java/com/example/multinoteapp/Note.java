package com.example.multinoteapp;

public class Note {
    private String myName;
    private String myUpdateTime;
    private String myText;

    public Note() {
        myName = null;
        myUpdateTime = null;
        myText = null;
    }

    public String getMyName() {
        return myName;
    }

    public void setName(String myNoteName) {
        this.myName = myNoteName;
    }

    public String getUpdateTime() {
        return myUpdateTime;
    }

    public void setUpdateTime(String myLastUpdateTime) {
        this.myUpdateTime = myLastUpdateTime;
    }

    public String getMyText() {
        return myText;
    }

    public void setMyText(String myText) {
        this.myText = myText;
    }
}


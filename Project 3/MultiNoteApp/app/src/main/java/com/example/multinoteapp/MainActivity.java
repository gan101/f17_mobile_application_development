package com.example.multinoteapp;


import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@TargetApi(21)
public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {

    private Adapter myAdapter;
    private RecyclerView myRecyclerView;
    private SimpleDateFormat myFormatter;
    private NoteListFragment myFragment;
    private static final int TAG = 1;
    private static final String FRAGMENT_TAG = "Fragment";
    private List<Note> myNotes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeRecyclerView();
        RunAsyncJob();
    }

    private void InitializeRecyclerView() {
        myFormatter = new SimpleDateFormat("EEE MMM dd, HH:mm:ss a");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myRecyclerView = (RecyclerView) findViewById(R.id.noteListView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void RunAsyncJob() {
        android.app.FragmentManager manager = getFragmentManager();
        myFragment = (NoteListFragment) manager.findFragmentByTag(FRAGMENT_TAG);
        if (myFragment != null) {
            myNotes = myFragment.getNoteList();
            myAdapter = new Adapter(myNotes, MainActivity.this);
            myRecyclerView.setAdapter(myAdapter);
        } else {
            MyAsyncTask task = new MyAsyncTask();
            task.execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionsmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.addNoteMenu:
                OnAddMenuClick();
                break;
            case R.id.infoMenu:
                OnInfoMenuClick();
                break;
        }
        return true;
    }

    private void OnAddMenuClick() {
        Intent intent;
        intent = new Intent(MainActivity.this, Edit.class);
        intent.putExtra(Intent.EXTRA_TEXT, MainActivity.class.getSimpleName());

        intent.putExtra(getString(R.string.noteIdentifierConst), getString(R.string.newNoteIdentifierConst));
        intent.putExtra(getString(R.string.notePositionConst), 0);
        intent.putExtra(getString(R.string.noteNameConst), "");
        intent.putExtra(getString(R.string.noteTextConst), "");

        startActivityForResult(intent, TAG);
    }

    private void OnInfoMenuClick() {
        Intent intent;
        intent = new Intent(MainActivity.this, About.class);
        intent.putExtra(Intent.EXTRA_TEXT, MainActivity.class.getSimpleName());
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String name = null;
        String text = null;
        String noteIdentifier = null;
        if (requestCode == TAG) {
            if (resultCode == RESULT_OK) {

                noteIdentifier = data.getStringExtra(getString(R.string.noteIdentifierConst));
                text = data.getStringExtra(getString(R.string.noteTextConst));
                name = data.getStringExtra(getString(R.string.noteNameConst));
                int position = data.getIntExtra(getString(R.string.notePositionConst), 0);

                Note note = new Note();
                note.setMyText(text);
                note.setUpdateTime(myFormatter.format(new Date()));
                note.setName(name);

                if (noteIdentifier.compareTo(getString(R.string.oldNoteIdentifierConst)) == 0) {
                    myNotes.remove(position);
                    position = 0;
                }
                myNotes.add(position, note);
                myAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View v) {
        int position = myRecyclerView.getChildLayoutPosition(v);
        Note note = myNotes.get(position);

        Intent intent = InitializeIntentForEditActivity(position, note);
        startActivityForResult(intent, TAG);
    }

    @NonNull
    private Intent InitializeIntentForEditActivity(int position, Note note) {
        Intent intent = new Intent(MainActivity.this, Edit.class);
        intent.putExtra(Intent.EXTRA_TEXT, MainActivity.class.getSimpleName());
        intent.putExtra(getString(R.string.noteNameConst), note.getMyName());
        intent.putExtra(getString(R.string.noteTextConst), note.getMyText());
        intent.putExtra(getString(R.string.notePositionConst), position);
        intent.putExtra(getString(R.string.noteIdentifierConst), getString(R.string.oldNoteIdentifierConst));
        return intent;
    }

    @Override
    public boolean onLongClick(View v) {
        final int position = myRecyclerView.getChildLayoutPosition(v);
        Note note = myNotes.get(position);

        ShowDialogForDeletion(position, note);
        return false;
    }

    private void ShowDialogForDeletion(final int position, Note note) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.deleteNoteDialogText) + " '" + note.getMyName() + "'?");

        builder.setPositiveButton(getString(R.string.dialogOptionYes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myNotes.remove(position);
                myAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton(getString(R.string.dialogOptionNo), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SaveNoteList();
    }

    private void SaveNoteList() {
        try {
            FileOutputStream stream = getApplicationContext().openFileOutput(getString(R.string.fileName), Context.MODE_PRIVATE);
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(stream, getString(R.string.encoding)));
            writer.setIndent(" ");
            writer.beginArray();
            for (Note note : myNotes) {
                writer.beginObject();
                writer.name(getString(R.string.noteNameConst)).value(note.getMyName());
                writer.name(getString(R.string.noteTextConst)).value(note.getMyText());
                writer.name(getString(R.string.noteUpdateTimeConst)).value(note.getUpdateTime());
                writer.endObject();
            }
            writer.endArray();
            writer.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static class NoteListFragment extends Fragment {
        private List<Note> myNotesList;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public void setData(List<Note> noteList) {
            this.myNotesList = noteList;
        }

        public List<Note> getNoteList() {
            return myNotesList;
        }
    }

    class MyAsyncTask extends AsyncTask<Long, String, List<Note>> {

        @Override
        protected void onPostExecute(List<Note> noteList) {
            if (noteList != null) {
                myNotes = noteList;
            }

            myAdapter = new Adapter(myNotes, MainActivity.this);
            myRecyclerView.setAdapter(myAdapter);

            SetFragment();
        }

        private void SetFragment() {
            android.app.FragmentManager manager = getFragmentManager();
            myFragment = (NoteListFragment) manager.findFragmentByTag(FRAGMENT_TAG);
            if (myFragment == null) {
                myFragment = new NoteListFragment();
                manager.beginTransaction().add(myFragment, FRAGMENT_TAG).commit();
                myFragment.setData(myNotes);
            }
        }

        private List<Note> loadFile() throws IOException {
            InputStream stream = getApplicationContext().openFileInput(getString(R.string.fileName));
            JsonReader reader = new JsonReader(new InputStreamReader(stream, getString(R.string.encoding)));
            try {
                return ReadNoteList(reader);
            } finally {
                reader.close();
            }
        }

        @Override
        protected List<Note> doInBackground(Long... params) {
            List<Note> noteList = null;
            try {
                noteList = loadFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return noteList;
        }

        private List<Note> ReadNoteList(JsonReader reader) throws IOException {
            List<Note> noteList = new ArrayList<Note>();
            reader.beginArray();
            while (reader.hasNext()) {
                Note note = new Note();
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals(getString(R.string.noteNameConst))) {
                        note.setName(reader.nextString());
                    } else if (name.equals(getString(R.string.noteTextConst))) {
                        note.setMyText(reader.nextString());
                    } else if (name.equals(getString(R.string.noteUpdateTimeConst))) {
                        note.setUpdateTime(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                noteList.add(note);
            }
            reader.endArray();
            return noteList;
        }
    }
}

package com.example.multinoteapp;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    private TextView name;
    private TextView updateTime;
    private TextView text;

    public RecyclerViewHolder(View view) {
        super(view);
        setNoteNameView((TextView) view.findViewById(R.id.cardNoteName));
        setLastUpdateTimeView((TextView) view.findViewById(R.id.updateTime));
        setNoteTextView((TextView) view.findViewById(R.id.noteText));
    }

    public TextView getNoteNameView() {
        return name;
    }

    public void setNoteNameView(TextView noteName) {
        this.name = noteName;
    }

    public TextView getLastUpdateTimeview() {
        return updateTime;
    }

    public void setLastUpdateTimeView(TextView lastUpdateTime) {
        this.updateTime = lastUpdateTime;
    }

    public TextView getNoteTextView() {
        return text;
    }

    public void setNoteTextView(TextView noteText) {
        this.text = noteText;
    }
}

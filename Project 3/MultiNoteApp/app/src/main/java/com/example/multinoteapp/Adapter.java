package com.example.multinoteapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    List<Note> myList;
    MainActivity myMainActivity;

    public Adapter(List<Note> notesList, MainActivity mainActivity) {
        myList = notesList;
        myMainActivity = mainActivity;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_list_row, parent, false);
        itemView.setOnClickListener(myMainActivity);
        itemView.setOnLongClickListener(myMainActivity);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Note note = myList.get(position);
        holder.getNoteNameView().setText(note.getMyName());
        holder.getLastUpdateTimeview().setText(note.getUpdateTime());

        String text = getTrimmedString(note);
        holder.getNoteTextView().setText(text);
    }

    @NonNull
    private String getTrimmedString(Note newNote) {
        String text = newNote.getMyText();
        if (text.length() > 80) {
            text = text.substring(0, 79) + "...";
        }
        return text;
    }

    @Override
    public int getItemCount() {
        return myList.size();
    }
}
